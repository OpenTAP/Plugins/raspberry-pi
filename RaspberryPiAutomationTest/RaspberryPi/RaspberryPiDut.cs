﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files..

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using OpenTap;
using Renci.SshNet;
using System.Net.NetworkInformation;

namespace RaspberryPiAutomationTest.RaspberryPi
{
    [Display("RaspberryPiDut", Groups: new[] { "RaspberryPi Automation Test", "RaspberryPi" }, Description: "Raspberry Pi DUT")]
    public class RaspberryPiDut : Dut
    {
        #region Settings
        public string Host { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public const string TURN_OFF_SCREEN = "xset -display :0.0 dpms force off";

        public const string TURN_ON_SCREEN = "xset -display :0.0 dpms force on";

        #endregion

        /// <summary>
        /// Initializes a new instance of this DUT class.
        /// </summary>
        public RaspberryPiDut()
        {
            Name = "Raspi";
            Host = "192.168.0.164";
            UserName = "pi";
            Password = "''";          
        }


        public void SendCommand(string command)
        {
            SshClient Ssh = new SshClient(Host, UserName, Password);
            Ssh.Connect();
            Ssh.RunCommand(command);
            Ssh.Disconnect();
        }

        public string SendCommandGetResult(string command)
        {
            SshClient Ssh = new SshClient(Host, UserName, Password);
            Ssh.Connect();
            SshCommand send = Ssh.CreateCommand(command);
            send.Execute();
            Ssh.Disconnect();
            return send.Result;            
        }

        public void CommandTurnOffScreen()
        {
            SshClient Ssh = new SshClient(Host, UserName, Password);
            Ssh.RunCommand(TURN_OFF_SCREEN);
        }

        public void CommandTurnOnScreen()
        {
            SshClient Ssh = new SshClient(Host, UserName, Password);
            Ssh.RunCommand(TURN_ON_SCREEN);
        }

        public bool PingDut()
        {
            var p1 = new Ping();

            // Wait 5 s for a connection
            var reply = p1.Send(Host);

            if (reply.Status == (IPStatus.Success))
            {
                Log.Info("Address: {0}", Host);
                Log.Info("Got reply after: {0} ms", reply.RoundtripTime);

                return true;
            }
            else
            {
                Log.Info("No reply from " + Host);
                Log.Info("Reply Status: " + reply.Status);

                return false;
            }
        }


        /// <summary>
        /// Opens a connection to the DUT represented by this class
        /// </summary>
        public override void Open()
        {
            base.Open();                
        }

        /// <summary>
        /// Closes the connection made to the DUT represented by this class
        /// </summary>
        public override void Close()
        {           
            base.Close();
        }
    }
}
