﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using OpenTap;

namespace RaspberryPiAutomationTest.RaspberryPi
{
    [Display("Generate Sine", Groups: new[] { "RaspberryPi Automation Test", "RaspberryPi" }, Description: "Insert a description here")]
    public class GenerateSineStep : TestStep
    {
        #region Settings
        public RaspberryPiDut Dut { get; set; }

        [Display("Input Frequency"), Unit("Hz")]
        [Output]
        public int Frequency { get; set; }
        #endregion

        public GenerateSineStep()
        {
            // ToDo: Set default values for properties / settings.
        }

        public override void Run()
        {
            string command = "play -V -r 192000 -n -b 16 -c 2 synth 15 sin " + Frequency + " vol -5dB";
            Dut.SendCommand(command);
        }
    }
}
