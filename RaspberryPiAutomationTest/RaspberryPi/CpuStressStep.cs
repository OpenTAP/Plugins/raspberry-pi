﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using OpenTap;

namespace RaspberryPiAutomationTest.RaspberryPi
{
    [Display("Cpu Stress", Groups: new[] { "RaspberryPi Automation Test", "RaspberryPi" }, Description: "Insert a description here")]
    public class CpuStressStep : TestStep
    {
        #region Settings
        public RaspberryPiDut Dut { get; set; }

        [Display("Stress Core Count")]
        [Output]
        public int CoreCount { get; set; }

        [Display("Test Duration"), Unit("s")]
        [Output]
        public double Duration { get; set; }
        #endregion

        public CpuStressStep()
        {
            // ToDo: Set default values for properties / settings.
        }

        public override void Run()
        {
            if
                (0 < CoreCount && CoreCount <= 4)
            {
                string command = "stress -c " + CoreCount + " --timeout " + Duration;
                Dut.SendCommand(command);
            }
            else
                UpgradeVerdict(Verdict.Fail);
        }
    }
}
