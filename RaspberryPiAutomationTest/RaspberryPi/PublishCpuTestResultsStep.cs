﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using OpenTap;

namespace RaspberryPiAutomationTest.RaspberryPi
{
    [Display("Publish Cpu Test Results", Group: "RaspberryPi Automation Test", Description: "Insert a description here")]
    public class PublishCpuTestResultsStep : TestStep
    {

        [Display("CPU Measurement")]
        public class MeasurementData
        {
            //[Display("Current before test ", Order: 1), Unit("A")]
            //public double CurrentBeforex { get; set; }

            [Display("Current start test ", Order: 2), Unit("A")]
            public double CurrentStartx { get; set; }

            [Display("Current end test ", Order: 3), Unit("A")]
            public double CurrentEndx { get; set; }

            [Display("Cores tested", Order: 4)]
            public int CoreCountx { get; set; }

            [Display("Test Duration", Order: 5)]
            public double TestDurationx { get; set; }

            [Display("Voltage", Order: 1), Unit("V")]
            public double VoltageX { get; set; }
        }
        #region Settings
        //[Display("Current before test ", Order: 1), Unit("A")]
        //public double CurrentBefore { get; set; }

        [Display("Current start test ", Order: 2), Unit("A")]
        public double CurrentStart { get; set; }

        [Display("Current end test ", Order: 3), Unit("A")]
        public double CurrentEnd { get; set; }

        [Display("Cores tested", Order: 4)]
        public int CoreCount { get; set; }
        #endregion

        [Display("Test Duration", Order: 5)]
        public double TestDuration { get; set; }

        [Display("Voltage", Order: 1), Unit("V")]
        public double Voltage { get; set; }

        public PublishCpuTestResultsStep()
        {
            // ToDo: Set default values for properties / settings.
        }

        public override void Run()
        {
            Results.Publish(new MeasurementData
            {
                //CurrentBeforex = CurrentBefore,
                VoltageX = Voltage,
                CurrentStartx = CurrentStart,
                CurrentEndx = CurrentEnd,
                CoreCountx = CoreCount,
                TestDurationx = TestDuration
            });
        }
    }
}
