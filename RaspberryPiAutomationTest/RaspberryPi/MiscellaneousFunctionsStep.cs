﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using OpenTap;

namespace RaspberryPiAutomationTest.RaspberryPi
{
    [Display("Miscellaneous Functions", Groups: new[] { "RaspberryPi Automation Test", "RaspberryPi" }, Description: "Insert a description here")]
    public class MiscellaneousFunctionsStep : TestStep
    {
        #region Settings
        public RaspberryPiDut Dut { get; set; }
        #endregion

        public MiscellaneousFunctionsStep()
        {
            // ToDo: Set default values for properties / settings.
        }

        [Display("Turn On Screen")]
        [Browsable(true)]
        public void TurnOnButton()
        {
            // This example logs a message on button click. However, anything can be done here, such as 
            // opening another application, calling a .bat file, etc.
            Dut.CommandTurnOnScreen();
            Log.Info("Button Clicked");
        }

        [Display("Turn Off Screen")]
        [Browsable(true)]
        public void TurnOffButton()
        {
            // This example logs a message on button click. However, anything can be done here, such as 
            // opening another application, calling a .bat file, etc.
            Dut.CommandTurnOffScreen();
            Log.Info("Button Clicked");
        }

        public override void Run()
        {
            // ToDo: Add test case code.
            RunChildSteps(); //If the step supports child steps.

            // If no verdict is used, the verdict will default to NotSet.
            // You can change the verdict using UpgradeVerdict() as shown below.
            // UpgradeVerdict(Verdict.Pass);
        }
    }
}
