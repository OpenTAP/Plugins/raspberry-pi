﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using OpenTap;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaspberryPiAutomationTest.RaspberryPi
{
    [Display("Validate Stress Test", Groups: new[] { "RaspberryPi Automation Test", "RaspberryPi" }, Description: "Insert a description here")]
    public class ValidateCpuResult : TestStep
    {
        #region Settings
        [Display("Current start test ", Order: 2), Unit("A")]
        public double CurrentStart { get; set; }

        [Display("Current end test ", Order: 3), Unit("A")]
        public double CurrentEnd { get; set; }

        [Browsable(true)]
        [Output]
        [Display("Test Verdict", Order: 7, Group: "Validation")]
        public Verdict SetVerdict { get; private set; }

        [Display("Max Current Threshold", Order: 3), Unit("A")]
        public double MaxCurrent { get; set; }
        #endregion

        public ValidateCpuResult()
        {
            MaxCurrent = 1.3;
        }

        public override void Run()
        {
            if
                (CurrentStart >= MaxCurrent || CurrentEnd >= MaxCurrent)
            {
                UpgradeVerdict(Verdict.Fail);
                SetVerdict = Verdict.Fail;
            }

            else
            {
                SetVerdict = Verdict.Pass;
                UpgradeVerdict(Verdict.Pass);
            }
        }
    }
}
