﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using OpenTap;
using System.Net;
using System.Net.NetworkInformation;

namespace RaspberryPiAutomationTest.RaspberryPi
{
    [Display("Ping Dut", Groups: new[] { "RaspberryPi Automation Test", "RaspberryPi" }, Description: "Insert a description here")]
    public class PingDutStep : TestStep
    {
        #region Settings
        public RaspberryPiDut Instrument { get; set; }
        #endregion

        public PingDutStep()
        {
        }

        public override void Run()
        {
            if
                (Instrument.PingDut() == true)
                UpgradeVerdict(Verdict.Pass);
            else
                UpgradeVerdict(Verdict.NotSet);
        }
    }
}
