﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using OpenTap;

namespace RaspberryPiAutomationTest.RaspberryPi
{
    [Display("Run Command", Groups: new[] { "RaspberryPi Automation Test", "RaspberryPi" }, Description: "Insert a description here")]
    public class SendCommandOnRunStep : TestStep
    {
        #region Settings
        public RaspberryPiDut Dut { get; set; }
        public string command { get; set; }
        #endregion

        public SendCommandOnRunStep()
        {
            // ToDo: Set default values for properties / settings.
        }

        public override void Run()
        {
            Dut.SendCommand(command);
        }
    }
}
