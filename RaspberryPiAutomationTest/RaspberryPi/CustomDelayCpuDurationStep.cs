﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using OpenTap;

namespace RaspberryPiAutomationTest.RaspberryPi
{
    [Display("Custom Delay CPU Stress", Groups: new[] { "RaspberryPi Automation Test", "RaspberryPi" }, Description: "Insert a description here")]
    public class CustomDelayCpuDurationStep : TestStep
    {
        #region Settings
        [Display("Test Duration"), Unit("s")]
        public double Duration { get; set; }

        [Display("Start Delay"), Unit("s")]
        public double StartDelay { get; set; }

        [Browsable(true)]
        [Display("Calculated Time Delay"), Unit("s")]

        public double EndDelay { get; private set; }

        #endregion

        public CustomDelayCpuDurationStep()
        {
            // ToDo: Set default values for properties / settings.
        }
       

        public override void Run()
        {
            EndDelay = Duration - StartDelay - 7;
            TapThread.Sleep(Convert.ToInt32(EndDelay)* 1000);
        }
    }
}
