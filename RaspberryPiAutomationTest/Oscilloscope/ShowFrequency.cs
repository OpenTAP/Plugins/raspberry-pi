﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using OpenTap;

namespace RaspberryPiAutomationTest.Oscilloscope
{
    [Display("Show Frequency", Groups: new[] { "RaspberryPi Automation Test", "Oscilloscope" }, Description: "Insert a description here")]
    public class ShowFrequency : TestStep
    {
        #region Settings

        [Browsable(true)]
        [Display("Output Frequency", Order: 1), Unit("Hz", UseEngineeringPrefix: true)]
        [Output]
        public double MeasuredFrequency { get; set; }

        #endregion

        public ShowFrequency()
        {
            // ToDo: Set default values for properties / settings.
        }

        public override void Run()
        {
            // ToDo: Add test case code.
            RunChildSteps(); //If the step supports child steps.

            // If no verdict is used, the verdict will default to NotSet.
            // You can change the verdict using UpgradeVerdict() as shown below.
            // UpgradeVerdict(Verdict.Pass);
        }
    }
}
