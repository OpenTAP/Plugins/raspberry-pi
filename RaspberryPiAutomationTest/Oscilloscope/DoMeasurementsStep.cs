﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using OpenTap;

namespace RaspberryPiAutomationTest.Oscilloscope
{
    [Display("Take Measurements", Groups: new[] { "RaspberryPi Automation Test", "Oscilloscope" }, Description: "Insert a description here")]
    public class DoMeasurementsStep : TestStep
    {
        #region Settings
        public OscilloscopeInstrument Instrument { get; set; }

        [Display("Input Frequency", Group: "Input"), Unit("Hz")]
        public int InputFrequency { get; set; } 

        [Browsable(true)]
        [Display("Output Frequency", Order: 1, Group: "Measurements"), Unit("Hz", UseEngineeringPrefix: true)]
        [Output]
        public double MeasuredFrequency { get; private set; }

        [Browsable(true)]
        [Display("Output Amplitude", Group: "Measurements"), Unit("V", UseEngineeringPrefix: true)]
        [Output]
        public double MeasuredAmplitude { get; private set; }

        [Browsable(true)]
        [Display("Output Period", Group: "Measurements"), Unit("s", UseEngineeringPrefix: true)]
        [Output]
        public double MeasuredPeriod { get; private set; }

        [Browsable(true)]
        [Display("Horizontal", Group: "Oscilloscope scale"), Unit("s", UseEngineeringPrefix: true)]
        public double Horizontal { get; private set; }

        [Browsable(true)]
        [Display("Vertical", Group: "Oscilloscope scale"), Unit("V", UseEngineeringPrefix: true)]
        public double Vertical { get; private set; }

        #endregion

        public DoMeasurementsStep()
        {
           
        }

        public void FindMeasurements()
        {
            if
    (0 < InputFrequency && InputFrequency <= 1)
            {
                Horizontal = 0.2;
                Vertical = 0.2;
            }
            else
            if
    (1 < InputFrequency && InputFrequency <= 5)
            {
                Horizontal = 0.05;
                Vertical = 0.2;
            }
            else
             if
    (5 < InputFrequency && InputFrequency <= 10)
            {
                Horizontal = 0.02;
                Vertical = 0.2;
            }
            else
            if
    (10 < InputFrequency && InputFrequency <= 50)
            {
                Horizontal = 0.01;
                Vertical = 0.2;
            }
            else
    if
    (50 < InputFrequency && InputFrequency <= 100)
            {
                Horizontal = 0.005;
                Vertical = 0.2;
            }
            else
    if
    (100 < InputFrequency && InputFrequency <= 500)
            {
                Horizontal = 0.001;
                Vertical = 0.5;
            }
            else
    if
    (500 < InputFrequency && InputFrequency <= 1000)
            {
                Horizontal = 0.0005;
                Vertical = 0.2;
            }
            else
            if
    (1000 < InputFrequency && InputFrequency <= 3000)
            {
                Horizontal = 0.0002;
                Vertical = 0.2;
            }
            else
    if
    (3000 < InputFrequency && InputFrequency <= 4000)
            {
                Horizontal = 0.0001;
                Vertical = 0.2;
            }
            else
    if
    (4000 < InputFrequency && InputFrequency <= 6000)
            {
                Horizontal = 0.0001;
                Vertical = 0.2;
            }
            else
                if
    (6000 < InputFrequency && InputFrequency <= 10000)
            {
                Horizontal = 0.00005;
                Vertical = 0.2;
            }
            else
             if
    (10000 < InputFrequency && InputFrequency <= 12000)
            {
                Horizontal = 0.00002;
                Vertical = 0.2;
            }
            else
            if
    (12000 < InputFrequency && InputFrequency <= 15000)
            {
                Horizontal = 0.00002;
                Vertical = 0.2;
            }
            else
            if
   (15000 < InputFrequency && InputFrequency <= 19000)
            {
                Horizontal = 0.00002;
                Vertical = 0.2;
            }
            else
            if
   (19000 < InputFrequency && InputFrequency <= 21000)
            {
                Horizontal = 0.00001;
                Vertical = 0.1;
            }
            else
            if
  (21000 < InputFrequency && InputFrequency <= 22000)
            {
                Horizontal = 0.00001;
                Vertical = 0.05;
            }
            else
            if
  (22000 < InputFrequency && InputFrequency <= 23000)
            {
                Horizontal = 0.00002;
                Vertical = 0.05;
            }
            else
            {
                Horizontal = 0.001;
                Vertical = 0.5;
            }
        }

        public void DoMeasurements()
        {
            //set Oscilloscope measurement by input frequency
            FindMeasurements();
            //result is this format:"1.07014E+00,1,1.99773878889E-04,1,5.00622810913E+03,1"
            string rawMeasurements = Instrument.DoMeasurements(Horizontal, Vertical);
                        
            string[] measurementsArray = rawMeasurements.Split(',');

            MeasuredAmplitude = Math.Round(double.Parse(measurementsArray[0]), 4, MidpointRounding.AwayFromZero);

            MeasuredPeriod = Math.Round(double.Parse(measurementsArray[2]), 7, MidpointRounding.AwayFromZero);

            MeasuredFrequency = Math.Round(double.Parse(measurementsArray[4]), 4, MidpointRounding.AwayFromZero);
        }

        public override void Run()
        {
            DoMeasurements();
        }
    }
}
