﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using OpenTap;

namespace RaspberryPiAutomationTest.Oscilloscope
{
    [Display("Calculate Difference", Groups: new[] { "RaspberryPi Automation Test", "Oscilloscope" }, Description: "Insert a description here")]
    public class CalculateDifferenceStep : TestStep
    {
        #region Settings
        [Display ("Instrument", Order: 1)]
        public OscilloscopeInstrument Instrument { get; set; }

        [Display("Input Frequency", Order: 2, Group: "Data"), Unit("Hz")]
        public int InputFrequency { get; set; }

        [Display("Output Frequency", Order: 4, Group: "Data"), Unit("Hz")]
        [Output]
        public double Output { get; set; }

        [Display("Difference", Order: 6, Group: "Validation"), Unit ("%")]
        [Output]
        public double Diff { get; set; }

        [Display("Max Accepted Difference", Order: 5, Group: "Data"), Unit ("%")]
        [Output]
        public double MaxDifference { get; set; }

        //[Display("Frequency Validation ", Order: 3, Group: "Data")]
        //public string Valid { get; set; }

        [Browsable(true)]
        [Output]
        [Display("Test Verdict", Order: 7, Group: "Validation")]
        public Verdict SetVerdict { get; private set; }
        #endregion

        public CalculateDifferenceStep()
        {
            MaxDifference = 1;
        }

        public float Percent()
        {
            double difference = Convert.ToDouble(InputFrequency) - Output;

            float x = (float) (Convert.ToSingle(difference) / Output);

            Diff = (Math.Round(x, 5) * 100);

            return x;

        }

        public override void Run()
        {
            double difference = Math.Abs(Percent()) * 100;

            if (MaxDifference > difference && difference > -MaxDifference)
            {
                UpgradeVerdict(Verdict.Pass);
                SetVerdict = Verdict.Pass;
            }
            else
            //    if
            //    (Valid == "Outside Specifications")
            //{
            //    UpgradeVerdict(Verdict.Inconclusive);
            //    SetVerdict = Verdict.Inconclusive; 
            //}
            //else
            {
                UpgradeVerdict(Verdict.Fail);
                SetVerdict = Verdict.Fail;
            }

            TapThread.Sleep(2000);
        }
    }
}
