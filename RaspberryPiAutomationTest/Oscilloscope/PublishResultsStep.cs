﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using OpenTap;

namespace RaspberryPiAutomationTest.Oscilloscope
{
    [Display("Publish Results", Groups: new[] { "RaspberryPi Automation Test", "Oscilloscope" }, Description: "Insert a description here")]
    public class PublishResultsStep : TestStep
    {
        #region Settings
        [Display("Measurement")]
        public class MeasurementData
        {

            [Display("Input Frequency", Order: 1, Group: "Input"), Unit("Hz")]
            public int InputFrequencyx { get; set; }

            [Display("Output Period", Order: 7, Group: "Measurements"), Unit("V")]
            public double OutputPeriodx { get; set; }

            [Display("Output Amplitude", Order: 8, Group: "Measurements"), Unit("s")]
            public double OutputAmplitudex { get; set; }

            [Display("Output Frequency", Order: 2, Group: "Measurements"), Unit("Hz")]
            [Output]
            public double Outputx { get; set; }

            [Display("Difference", Order: 3, Group: "Validation"), Unit("%")]
            public double Diffx { get; set; }

            [Display("Max Accepted Difference", Order: 4, Group: "Input"), Unit("%")]
            public double MaxDifferencex { get; set; }

            [Display("Test Verdict", Order: 5, Group: "Validation")]
            public Verdict SetVerdictx { get; set; }

            //[Display("Frequency Validation ", Order: 6, Group: "Input")]
            //public string Validx { get; set; }

            public MeasurementData()
            {

            }
            #endregion
        }

        [Display("Input Frequency", Order: 1, Group: "Input"), Unit("Hz")]
        public int InputFrequency { get; set; }

        [Display("Output Period", Order: 7, Group: "Measurements"), Unit("V")]
        public double OutputPeriod { get; set; }

        [Display("Output Amplitude", Order: 8, Group: "Measurements"), Unit("s")]
        public double OutputAmplitude { get; set; }

        [Display("Output Frequency", Order: 2, Group: "Measurements"), Unit("Hz")]
        public double Output { get; set; }

        [Display("Frequency Difference", Order: 3, Group: "Validation"), Unit("%")]
        public double Diff { get; set; }

        [Display("Max Accepted Difference", Order: 4, Group: "Input"), Unit("%")]
        public double MaxDifference { get; set; }

        [Display("Test Verdict", Order: 6, Group: "Validation")]
        public Verdict SetVerdict { get; set; }

        //[Display("Frequency Validation ", Order: 5, Group: "Input")]
        //public string Valid { get; set; }
        public PublishResultsStep()
        {
            // ToDo: Set default values for properties / settings.
        }

        public override void Run()
        {
            Results.Publish(new MeasurementData
            {
                InputFrequencyx = InputFrequency,            
                Outputx = Output,
                Diffx = Diff,
                MaxDifferencex = MaxDifference,
                //Validx = Valid,
                SetVerdictx = SetVerdict,
                OutputAmplitudex = OutputAmplitude,
                OutputPeriodx = OutputPeriod
            });
        }
    }
}
