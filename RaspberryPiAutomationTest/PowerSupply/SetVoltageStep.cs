﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using OpenTap;

namespace RaspberryPiAutomationTest.PowerSupply
{
    [Display("Set Voltage", Groups: new[] { "RaspberryPi Automation Test", "Power Supply" }, Description: "Insert a description here")]
    public class SetVoltageStep : TestStep
    {
        #region Settings
        [Display ("Power Supply", Order: 1)]
        public PowerSupplyInstrument Instrument { get; set; }

        [Display("Voltage", Order: 1), Unit ("V")]
        [Output]
        public double Voltage { get; set; }

        [Display("Power Supply Output", Order: 2)]

        public PowerSupplyOutput OutputSelection { get; set; }
        #endregion

        public SetVoltageStep()
        {
            Voltage = 5.0;
            OutputSelection = PowerSupplyOutput.Out1;
        }

        public enum PowerSupplyOutput
        {
            [Display("Output 1")]
            Out1,
            [Display("Output 2")]
            Out2,
        }

        public string SetOutputPowerSupply()
        {
            if
                (OutputSelection == PowerSupplyOutput.Out1)
                return "OUTput1";
            else
                if
                (OutputSelection == PowerSupplyOutput.Out2)
                return "OUTput2";
            else
            {
                UpgradeVerdict(Verdict.Aborted);
                return "";
            }
        }

        public override void Run()
        {
            Instrument.SetOutputPowerSupply(SetOutputPowerSupply());
            TapThread.Sleep(500);
            Instrument.SetVoltage(Voltage);
        }
    }
}
