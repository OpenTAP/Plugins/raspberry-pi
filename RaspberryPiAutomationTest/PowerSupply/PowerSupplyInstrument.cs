﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using OpenTap;

//Note this template assumes that you have a SCPI based instrument, and accordingly
//extends the ScpiInstrument base class.

//If you do NOT have a SCPI based instrument, you should modify this instance to extend
//the (less powerful) Instrument base class.

namespace RaspberryPiAutomationTest.PowerSupply
{
    [Display("Power Supply", Groups: new[] { "RaspberryPi Automation Test", "Power Supply" }, Description: "Insert a description here")]
    public class PowerSupplyInstrument : ScpiInstrument
    {
        #region Settings
        // ToDo: Add property here for each parameter the end user should be able to change
        #endregion

        public PowerSupplyInstrument()
        {
            Name = "Power Supply";
            VisaAddress = "ASRL6::INSTR";
        }

        /// <summary>
        /// Open procedure for the instrument.
        /// </summary>
        /// 
        public void SetVoltage (double voltage)
        {
            ScpiCommand("OUTPut:STATe 1");
            ScpiCommand("SOURce:VOLTage:LEVel:IMMediate:AMPLitude " + voltage);
            TapThread.Sleep(500);
        }

        public void StopVoltage()
        {
            ScpiCommand("OUTPut:STATe 0");
        }

        public void SetOutputPowerSupply(string outputSelection)
        {
            ScpiCommand("INSTrument:SELect " + outputSelection);
            TapThread.Sleep(500);
        }

        public double GetVoltage()
        {
            TapThread.Sleep(200);
            return Convert.ToDouble(ScpiQuery("MEASure:SCALar:VOLTage:DC?"));

        }

        public double GetCurrent()
        {            
            TapThread.Sleep(200);
            return Convert.ToDouble(ScpiQuery("MEASure:SCALar:CURRent:DC?"));
            
        }
        public override void Open()
        {
            base.Open();
        }

        /// <summary>
        /// Close procedure for the instrument.
        /// </summary>
        public override void Close()
        {
            ScpiCommand("OUTPut:STATe 0");
            base.Close();
        }
    }
}
