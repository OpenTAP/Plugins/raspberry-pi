﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using OpenTap;

namespace RaspberryPiAutomationTest.PowerSupply
{
    [Display("Measure Voltage", Groups: new[] { "RaspberryPi Automation Test", "Power Supply" }, Description: "Insert a description here")]
    public class MeasureVoltageStep : TestStep
    {
        #region Settings
        [Display("Power Supply", Order: 1)]
        public PowerSupplyInstrument Instrument { get; set; }

        [Display("Measured Voltage", Order: 2), Unit("V")]
        [Browsable(true)]
        [Output]
        public double Voltage { get; private set; }

        [Display("Power Supply Output", Order: 2)]
        public PowerSupplyOutput OutputSelection { get; set; }
        #endregion

        public MeasureVoltageStep()
        {
            OutputSelection = PowerSupplyOutput.Out1;
        }

        public enum PowerSupplyOutput
        {
            [Display("Output 1")]
            Out1,
            [Display("Output 2")]
            Out2,
        }

        public string SetOutputPowerSupply()
        {
            if
                (OutputSelection == PowerSupplyOutput.Out1)
                return "OUTput1";
            else
                if
                (OutputSelection == PowerSupplyOutput.Out2)
                return "OUTput2";
            else
            {
                UpgradeVerdict(Verdict.Aborted);
                return "";
            }
        }

        public override void Run()
        {
            Instrument.SetOutputPowerSupply(SetOutputPowerSupply());
            TapThread.Sleep(500);
            Voltage = Math.Round(Instrument.GetVoltage(), 4, MidpointRounding.AwayFromZero);
        }
    }
}
