﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using OpenTap;

namespace RaspberryPiAutomationTest.Digit_Multimeter
{
    [Display("Publish Results", Groups: new[] { "RaspberryPi Automation Test", "Digit Multimeter" }, Description: "Insert a description here")]
    public class PublishResultsStep : TestStep
    {
        public class MeasurementData
        {
            [Display("Input Voltage", Order: 1), Unit("V")]
            public double InputVoltagex { get; set; }

            [Display("PSU Measured Voltage", Order: 2), Unit("V")]
            [Output]
            public double PsuVoltagex { get; set; }

            //[Display("DMM Measured Voltage", Order: 3), Unit("V")]
            //[Output]
            //public double DmmVoltagex { get; set; }

            [Display("PSU Measured Current", Order: 4), Unit("A")]
            [Output]
            public double PsuCurrentx { get; set; }

            [Display("Power", Order: 5), Unit("W")]
            public double Powerx { get; set; }

            //[Display("Polyfuse Voltage Drop", Order: 6), Unit("V")]
            //[Output]
            //public double PolyfuseVoltagex { get; set; }

            //[Display("Polyfuse Verdict", Order: 7)]
            //public Verdict PolyfuseVerdictx { get; set; }

            [Display("Device is ", Order: 8)]
            public string DeviceIsx { get; set; }

            [Display("On/Off Verdict", Order: 9)]
            public Verdict OnOffVerdictx { get; set; }

            public MeasurementData()
            {

            }
        }
        #region Settings
        [Display("Input Voltage", Order: 1), Unit("V")]
        public double InputVoltage { get; set; }

        [Display("PSU Measured Voltage", Order: 2), Unit("V")]
        [Output]
        public double PsuVoltage { get; set; }

        //[Display("DMM Measured Voltage", Order: 3), Unit("V")]
        //[Output]
        //public double DmmVoltage { get; set; }

        [Display("PSU Measured Current", Order: 4), Unit("A")]
        [Output]
        public double PsuCurrent { get; set; }

        [Display("Power", Order: 5), Unit("W")]
        public double Power { get; set; }

        //[Display("Polyfuse Voltage Drop", Order: 6), Unit("V")]
        //[Output]
        //public double PolyfuseVoltage { get; set; }

        //[Display("Polyfuse Verdict", Order: 7)]
        //public Verdict PolyfuseVerdict { get; set; }

        [Display("Device is ", Order: 7)]
        public string DeviceIs { get; set; }


        [Display("On/Off Verdict", Order: 9)]
        public Verdict OnOffVerdict { get; set; }


        #endregion

        public PublishResultsStep()
        {
            // ToDo: Set default values for properties / settings.
        }

        public override void Run()
        {

            Power = Math.Round(PsuVoltage * PsuCurrent, 4, MidpointRounding.AwayFromZero);

            Results.Publish(new MeasurementData
            {
                InputVoltagex = InputVoltage,
                PsuVoltagex = PsuVoltage,
               // DmmVoltagex = DmmVoltage,
                PsuCurrentx = PsuCurrent,
                Powerx = Power,
                //PolyfuseVoltagex = PolyfuseVoltage,
                //PolyfuseVerdictx = PolyfuseVerdict,
                DeviceIsx = DeviceIs,            
                OnOffVerdictx = OnOffVerdict
            });
        }
    }
}
