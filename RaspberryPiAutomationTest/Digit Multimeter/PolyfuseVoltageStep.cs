﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using OpenTap;

namespace RaspberryPiAutomationTest.Digit_Multimeter
{
    [Display("Polyfuse Voltage Drop", Groups: new[] { "RaspberryPi Automation Test", "Digit Multimeter" }, Description: "Insert a description here")]
    public class PolyfuseVoltageStep : TestStep
    {
        #region Settings
        [Display("Measured Voltage PSU"), Unit("V")]
        public double VoltagePSU { get; set; }

        [Display("Measured Voltage DMM"), Unit("V")]
        public double VoltageDMM { get; set; }
        #endregion

        [Display("Polyfuse Voltage Drop"), Unit("V")]
        [Output]
        public double VoltageDifference { get;set; }

        [Browsable(true)]
        [Output]
        [Display("Test Verdict", Order: 6)]
        public Verdict SetVerdict { get; private set; }

        public PolyfuseVoltageStep()
        {
            // ToDo: Set default values for properties / settings.
        }

        public override void Run()
        {
            VoltageDifference = Math.Round(VoltagePSU - VoltageDMM, 4, MidpointRounding.AwayFromZero);

            if (VoltageDifference < 0.3)
            {
                UpgradeVerdict(Verdict.Pass);
                SetVerdict = Verdict.Pass;
            }
            else
            {
                UpgradeVerdict(Verdict.Fail);
                SetVerdict = Verdict.Fail;
            }
        }
    }
}
