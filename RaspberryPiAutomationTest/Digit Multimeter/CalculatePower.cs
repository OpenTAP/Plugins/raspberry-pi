﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using OpenTap;

namespace RaspberryPiAutomationTest.Digit_Multimeter
{
    [Display("Calculate Power", Groups: new[] { "RaspberryPi Automation Test", "Digit Multimeter" }, Description: "Insert a description here")]
    public class CalculatePower : TestStep
    {
        #region Settings
        [Display("Voltage measured", Order: 1), Unit("V")]
        [Browsable(true)]
        [Output]
        public double Voltage { get; set; }

        [Display("Current measured", Order: 2), Unit("A")]
        [Browsable(true)]
        [Output]
        public double Current { get; set; }

        [Display("Power used", Order: 3), Unit("W")]
        [Output]
        public double Power { get; set; }
        #endregion

        public CalculatePower()
        {
            // ToDo: Set default values for properties / settings.
        }

        public override void Run()
        {           
            Power = Math.Round(Voltage * Current, 4, MidpointRounding.AwayFromZero);
        }
    }
}
