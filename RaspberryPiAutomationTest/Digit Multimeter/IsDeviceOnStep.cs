﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using OpenTap;

namespace RaspberryPiAutomationTest.Digit_Multimeter
{
    [Display("Is Device On?", Groups: new[] { "RaspberryPi Automation Test", "Digit Multimeter" }, Description: "Insert a description here")]
    public class IsDeviceOnStep : TestStep
    {
        #region Settings
        [Display("Current measured"), Unit("A")]
        [Output]
        public double Current { get; set; }

        [Display("Device is")]
        [Output]
        public string DeviceIsOn { get; set; }
        #endregion

        [Browsable(true)]
        [Output]
        [Display("Test Verdict", Order: 6)]
        public Verdict SetVerdict { get; private set; }

        public IsDeviceOnStep()
        {
            // ToDo: Set default values for properties / settings.
        }

        public override void Run()
        {
            if (Current < 0.1)
            {
                DeviceIsOn = "Off";
                UpgradeVerdict(Verdict.Fail);
                SetVerdict = Verdict.Fail;
            }
            else
                if
                (0.1 < Current && Current < 2)
            {
                DeviceIsOn = "On";
                UpgradeVerdict(Verdict.Pass);
                SetVerdict = Verdict.Pass;
            }
            else
            {
                DeviceIsOn = "Mabye";
                UpgradeVerdict(Verdict.Inconclusive);
                SetVerdict = Verdict.Inconclusive;
            }    
        }
    }
}
