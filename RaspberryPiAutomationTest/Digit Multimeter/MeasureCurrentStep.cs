﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using OpenTap;

namespace RaspberryPiAutomationTest.Digit_Multimeter
{
    [Display("Measure Current", Groups: new[] { "RaspberryPi Automation Test", "Digit Multimeter" }, Description: "Insert a description here")]
    public class MeasureCurrentStep : TestStep
    {
        #region Settings
        public DmmInstrument Instrument { get; set; }

        [Display("Measured Current"), Unit("A")]
        [Browsable(true)]
        [Output]
        public double Current { get; private set; }
        #endregion

        public MeasureCurrentStep()
        {
            // ToDo: Set default values for properties / settings.
        }

        public override void Run()
        {            
            Current = Math.Round(Instrument.GetCurrent() * 1000, 4, MidpointRounding.AwayFromZero);
        }
    }
}
